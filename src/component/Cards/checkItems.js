import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import { TrashIcon } from "chakra-ui-ionicons";
import { AddIcon } from "chakra-ui-ionicons";
import { Text, Input, Button, Flex, Portal } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";

class CheckItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkItems: [],
      isOpen: false,
      checkItemsName: "",
      isOpenCheckItems: false,
      checkListName: "",
    };
  }
  componentDidMount() {
    const id = this.props.id;
    console.log(id);
    TrelloApi.displayCheckItems(id)
      .then((res) => {
        this.setState({
          checkItems: res,
          ColorMode: null,
        });
      })
      .catch((err) => {
        this.setState({
          err,
        });
      });
  }

  render() {
    return this.state.checkItems.map((item) => {
      <Text>{item.name}</Text>;
    });
  }
}

export default CheckItems;

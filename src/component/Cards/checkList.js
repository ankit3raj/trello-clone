import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import { TrashIcon } from "chakra-ui-ionicons";
import { AddIcon } from "chakra-ui-ionicons";
import { Text, Input, Button, Flex, Portal } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";

import { Checkbox, CheckboxGroup } from "@chakra-ui/react";
import { Progress } from "@chakra-ui/react";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkList: [],
      isOpen: false,
      cardName: "",
      isOpenCheckList: false,
      checkListName: "",
      currentCheckListId: "",
      checkItemsName: "",
      checkedItems: "",
    };
  }
  componentDidMount() {
    const id = this.props.id;
    console.log(id);
    TrelloApi.displayCheckListByCardId(id)
      .then((res) => {
        this.setState({
          checkList: res,
          ColorMode: null,
        });
      })
      .catch((err) => {
        this.setState({
          err,
        });
      });
  }

  handleClickForCheckItems = (id) => {
    this.setState({ currentCheckListId: id });
  };

  handleClickForCheckList = () => {
    this.setState({ isOpenCheckList: true });
  };
  handleClickForCheckListClose = () => {
    this.setState({ isOpenCheckList: false });
  };
  handleClose = () => {
    this.setState({ isOpen: false });
  };
  handleSubmitClick = () => {
    TrelloApi.createCheckList(this.props.id, this.state.checkListName).then(
      (res) => {
        const newCheckList = this.state.checkList.concat(res);
        this.setState({ checkList: newCheckList, isOpenCheckList: false });
      }
    );
  };
  handleDelete = (id) => {
    TrelloApi.deleteCheckList(id).then((res) => {
      const newData = this.state.checkList.filter((item) => item.id !== id);
      this.setState({ checkList: newData });
    });
  };
  handleCreate = (id) => {
    TrelloApi.createCheckItems(id, this.state.checkItemsName).then((res) => {
      const newData = this.state.checkList.reduce((acc, curr) => {
        if (curr.id === res.idChecklist) {
          curr.checkItems.push(res);
        }
        acc.push(curr);
        return acc;
      }, []);
      this.setState({ checkList: newData });
    });
  };
  handleDeleteItems = (idCheckList, idCheckItems) => {
    TrelloApi.deleteCheckItems(idCheckList, idCheckItems).then((res) => {
      const newCheckList = this.state.checkList.map((e) =>
        e.id === idCheckList
          ? {
              ...e,
              items: e.items.filter((inner) => inner.id !== idCheckItems),
            }
          : e
      );
      this.setState({ checkList: newCheckList });
    });
  };
  countChecked = (id) => {
    this.state.checkList.reduce((acc, curr) => {});
  };

  render() {
    return (
      <>
        <ModalContent>
          <ModalHeader></ModalHeader>
          <ModalCloseButton />

          <ModalBody>
            <Text as="h1" fontWeight="bold">
              Card-Name: {this.props.name}
            </Text>
          </ModalBody>
          <ModalBody>
            <Button onClick={this.handleClickForCheckList}>CheckList</Button>
            <Modal isOpen={this.state.isOpenCheckList}>
              <ModalOverlay />

              <ModalContent>
                <ModalHeader> Create Your CheckList</ModalHeader>
                <ModalCloseButton />

                <ModalBody>
                  <Input
                    placeholder=" Your checkList Name"
                    onChange={(e) => {
                      this.setState({ checkListName: e.target.value });
                    }}
                  />
                </ModalBody>

                <ModalFooter>
                  <Button
                    colorScheme="red"
                    mr={3}
                    onClick={this.handleClickForCheckListClose}
                  >
                    Close
                  </Button>
                  <Button colorScheme="blue" onClick={this.handleSubmitClick}>
                    Create your CheckList
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </ModalBody>
          {this.state.checkList.map((item) => {
            return (
              <>
                <Flex justify="space-between">
                  <ModalBody>{item.name}</ModalBody>
                  <TrashIcon
                    w={6}
                    h={6}
                    onClick={() => {
                      this.handleDelete(item.id);
                    }}
                  />

                  <Input
                    placeholder="CardItems Name"
                    onChange={(e) =>
                      this.setState({ checkItemsName: e.target.value })
                    }
                  />
                  <Button
                    onClick={() => {
                      this.handleCreate(item.id);
                    }}
                  >
                    Add Item
                  </Button>
                </Flex>
                <Flex direction="column">
                  <Text>{(100 / item.checkItems.length).toFixed(2)}</Text>
                  <Progress
                    m={2}
                    value={
                      item.checkItems
                        .filter((item) => item.state === "complete")
                        .length.toFixed(2) / 100
                    }
                  />
                  {item.checkItems.map((ele) => {
                    return (
                      <Flex justify="space-between">
                        <Checkbox
                          onChange={(e) => {
                            this.setState({ checkedItems: e.target.checked });
                          }}
                        >
                          <Text>{ele.name}</Text>
                        </Checkbox>

                        <TrashIcon w={6} h={6} />
                      </Flex>
                    );
                  })}
                </Flex>
              </>
            );
          })}

          <ModalFooter>
            <Button colorScheme="red" mr={3} onClick={this.props.handleClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </>
    );
  }
}

export default CheckList;
